package geometry;

public class Triangle {
    private double side;
    private double height;

    public Triangle(double side, double height){
        this.side = side;
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getSide() {
        return side;
    }

    public double getArea(){
        double area = (this.side * this.height) / 2;
        return area;
    }
    
    public String toString(){
        String stringHolder = "Sides " + side + "Height " + height;
        return stringHolder;
    }

}
