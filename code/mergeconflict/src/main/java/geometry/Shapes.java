package geometry;

public class Shapes {
    public static void main(String[]args){
        Triangle myTriangle = new Triangle(5, 6);
        System.out.println(myTriangle);
        System.out.println(myTriangle.getArea());

        Circle myCircle = new Circle(5);
        System.out.println(myCircle);
        System.out.println(myCircle.getArea());
    }
}
