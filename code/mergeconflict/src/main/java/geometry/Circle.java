package geometry;

public class Circle {
    private double radius;

    public Circle(double radius){
        this.radius = radius;
    }
    
    public double getArea(){
        return Math.PI * Math.pow(radius, 2);
    }

    public String toString(){
        return "Circle of radius " + this.radius;
    }

    public double getRadius(){
        return this.radius;
    }
}
