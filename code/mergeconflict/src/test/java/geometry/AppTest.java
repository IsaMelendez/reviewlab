package geometry;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testingGetters()
    {
        Triangle triangle = new Triangle(5, 6);
    }

    @Test
    public void circleGetters()
    {
        Circle c = new Circle(5);
        assertEquals(5, c.getRadius(), 0.001);
    }

    @Test
    public void circleArea()
    {
        Circle c = new Circle(5);
        assertEquals(78.54, c.getArea(), 0.001);
    }

    @Test
    public void circleString()
    {
        Circle c = new Circle(5);
        assertEquals("Circle of radius 5.0", c.toString());
    }
}
